<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles/styles.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
        <script src="./scripts/main.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
        <meta charset="UTF-8">
    </head>
    <body onload="onLoad()">
        <canvas id="GUICanvas">
        Your browser does not support the canvas element.
        </canvas>
    </body>
</html>

