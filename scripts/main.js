var worldWidth = 4*1280;
var worldHeight = 4*720;
var seaColor = '00ff00';
var fish = [];
var maxFish = 25;
var maxRadius = 5;
var maxSeeingDistance = 1000;
var maxSwimSpeed = 5;
var maxComfortZone = 10;
var twopi = Math.PI*2;
var foodList = [];

function onLoad() {
	canvas = document.getElementById("GUICanvas");
	canvas.width = worldWidth;
	canvas.height = worldHeight;
	ctx = canvas.getContext("2d");
	loadVariables();
	for (i=0; i<maxFish; i++){fish[i] = generateFish();}
	window.requestAnimationFrame(gameLoop);
}
function generateFish(){
	localFish = {x:Math.random()*worldWidth,
		y:Math.random()*worldHeight,
		radius:25, //Math.random()*maxRadius,
		fillStyle:Math.floor(Math.random()*255).toString(16) ,//"#FFAA00",
		seeingDistance:Math.random()*maxSeeingDistance,
		swimSpeed:Math.random()*maxSwimSpeed,
		targetx:this.x,
		targety:this.y,
		comfortZone:Math.random()*maxComfortZone,
		swimDirection:Math.random()*2*Math.PI,
		foodList:[],
		clock:64000,
		seeingList:[]
	}
	localFish.comfortZone = localFish.comfortZone % localFish.seeingDistance;
	return localFish;
}
function generateFood(){
	var x = Math.random()*worldWidth;
	var y = Math.random()*worldHeight;
	foodList.push({x:x,y:y});
}
function loadVariables(){

}
function gameLoop(){
	if(Math.random()>0.99){generateFood();}
	fishSee();
	moveFish();
	draw();
	window.requestAnimationFrame(gameLoop);
}
function fishSee(){
    oldFish = fish;
    for (i=0; i<fish.length; i++){
        fish[i].foodList.length = 0;
        fish[i].seeingList.length = 0;
        for (j=0; j<foodList.length; j++){
            xdist = fish[i].x - foodList[j].x;
            ydist = fish[i].y - foodList[j].y;
            absDist = Math.sqrt((xdist*xdist) + (ydist*ydist));
            if (absDist < fish[i].seeingDistance){
                fish[i].foodList.push({x:foodList[j].x, y:foodList[j].y, dist:absDist});
                if(((Math.abs(fish[i].x - foodList[j].x))<fish[i].radius) && ((Math.abs(fish[i].y - foodList[j].y))<fish[i].radius)){foodList.splice(j, 1);}
            }
        }
        for (j=0; (j<oldFish.length); j++){
            if (j != i){
                xdist = fish[i].x - oldFish[j].x;
                ydist = fish[i].y - oldFish[j].y;
                absDist = Math.sqrt((xdist*xdist) + (ydist*ydist));
                if (absDist < fish[i].seeingDistance){
                    fish[i].seeingList.push({x:fish[j].x, y:fish[j].y, dist:absDist, swimDirection:oldFish[j].swimDirection, conformity:oldFish[j].conformity});
                }
            }
        }
    }
}
function draw(){
	drawSea();
	drawFood();
	drawFish();
}
function drawSea(){
	ctx.fillStyle =  "#339CFF";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}
function drawFish(){
	for (i=0;i<fish.length;i++){
		a = (Math.PI*2 + fish[i].swimDirection) % (Math.PI*2);
		s = (Math.floor(a*255/twopi)).toString(16);
	        while (s.length<2){s = "0" + s;}
		drawCircle({x:fish[i].x, y:fish[i].y}, fish[i].radius, "#" + fish[i].fillStyle + s + "00");
		ctx.lineWidth = 1;
		drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[i].targetx, y:fish[i].targety}, "#ff0000");
		ctx.beginPath();
		ctx.arc(fish[i].x, fish[i].y, fish[i].seeingDistance, 0, twopi, true);
		ctx.strokeStyle = "#000000";
		ctx.lineWidth = 1;
		ctx.stroke();
		for (j=0; j<fish[i].seeingList.length; j++){
		    drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[i].seeingList[j].x, y:fish[i].seeingList[j].y}, "#00ffff");
		}
		for (j=0; j<fish[i].foodList.length; j++){
		    drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[i].foodList[j].x, y:fish[i].foodList[j].y}, "#ffff00");
		}
		if (fish[i].x<fish[i].seeingDistance){
            drawLine({x:fish[i].x, y:fish[i].y}, {x:0, y:fish[i].y}, "#ff0000");   
        }
        if (fish[i].y<fish[i].seeingDistance){
            drawLine({x:fish[i].x, y:fish[i].y}, {y:0, x:fish[i].x}, "#ff0000");   
        }
        if (fish[i].x>(worldWidth - fish[i].seeingDistance)){
            drawLine({x:fish[i].x, y:fish[i].y}, {x:worldWidth, y:fish[i].y}, "#ff0000");   
        }
        if (fish[i].y>(worldHeight - fish[i].seeingDistance)){
            drawLine({x:fish[i].x, y:fish[i].y}, {y:worldHeight, x:fish[i].x}, "#ff0000");   
        }
	}
}
function drawCircle(a, radius, color){
	ctx.beginPath();
	ctx.arc(a.x, a.y, radius, 0, twopi, true);
	ctx.fillStyle = color;
	ctx.fill();
}
function drawLine(a, b, color){
	ctx.beginPath();
	ctx.moveTo(a.x, a.y);
	ctx.lineTo(b.x, b.y);
	ctx.strokeStyle = color;
	ctx.stroke();
}
function drawFood(){
    for (i=0; i<foodList.length; i++){
        drawCircle({x:foodList[i].x, y:foodList[i].y}, 25, "#00ff00");
    }
}
function moveFish(){
	for (i=0; i<fish.length; i++){
	    if ((fish[i].x < (0)) || (fish[i].x > (worldWidth))){fish[i].x = (worldWidth + fish[i].x) % worldWidth;}
        if ((fish[i].y < (0)) || (fish[i].y > (worldHeight))){fish[i].y = (worldHeight + fish[i].y) % worldHeight;}
		fish[i].x = fish[i].x + Math.cos(fish[i].swimDirection)*fish[i].swimSpeed;
		fish[i].y = fish[i].y + Math.sin(fish[i].swimDirection)*fish[i].swimSpeed;
		fish[i].targetx = fish[i].x + Math.cos(fish[i].swimDirection)*200;
		fish[i].targety = fish[i].y + Math.sin(fish[i].swimDirection)*200;
	}
}
